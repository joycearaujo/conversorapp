import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  Keyboard
} from 'react-native';

export default class App extends Component{

  constructor(props){
    super(props);
    this.state={
      moedaA: props.moedaA,
      moedaB: props.moedaB,
      moedaB_valor: 0,
      valorConvertido: 0
    };

    this.converter = this.converter.bind(this);
  }

  //url api : https://free.currencyconverterapi.com/api/v5/convert?q=USD_BRL&compact=ultra&apiKey=4bd948d4a0e18950d43c

  converter(){

    let de_para = this.state.moedaA+ '_' + this.state.moedaB;
    this.url = 'https://free.currencyconverterapi.com/api/v5/convert?q='+de_para+'&compact=ultra&apiKey=4bd948d4a0e18950d43c';

    fetch(this.url)
    .then((response) => response.json())
    .then((rjson)=>{
      let state = this.state
      let cotacao = rjson[de_para];
      state.valorConvertido = (cotacao * parseFloat(this.state.moedaB_valor)).toFixed(2);
      this.setState(state);
    })

    Keyboard.dismiss();

  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titulo}>USD para BRL</Text>

        <TextInput 
          placeholder="USD"
          style={styles.areaInput}
          underlineColorAndroid="transparent"
          keyboardType="numeric"
          onChangeText={(moedaB_valor)=> this.setState({moedaB_valor})}
        />

        <TouchableHighlight style={styles.areaBotao} onPress={this.converter}>
          <Text style={styles.botaoTexto}>Converter</Text>
        </TouchableHighlight>

        <Text style={styles.valorConvertido}>{(this.state.valorConvertido ==0)? '' : this.state.valorConvertido}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titulo:{
    fontSize: 30,
    color: '#000',
    fontWeight: 'bold'
  },
  areaInput:{
    width: 280,
    height: 45,
    backgroundColor: '#CCC',
    borderRadius: 5,
    marginTop: 15,
    textAlign: 'center',
    fontSize: 20
  },
  areaBotao:{
    height: 45,
    width: 150,
    backgroundColor: '#FF0000',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15
  },
  botaoTexto:{
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFF'
  }, 
  valorConvertido:{
    marginTop: 15,
    fontSize: 30,
    fontWeight: 'bold',
    color: '#000'
  }
})

